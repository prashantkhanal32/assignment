/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    colors: {
      primary: '#0658C0',
      success: '#32BF52',
      warning: '#E22B41',
      'pure-white': '#FFFFFF',
      link: '#192FA4',
      'text-black': '#1E2630',
      yellow: '#F9A119',
      'light-grey': '#F8F9FD',
      'border-color': '#DCDCEB',
      'light-black': '#7A7D9C',
    },
    extend: {},
  },
  plugins: [],
};
