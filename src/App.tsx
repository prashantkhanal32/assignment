import React from 'react';
import { Home } from './pages';

interface IProps {}

const App: React.FC<IProps> = ({}) => {
  return <Home />;
};

export default React.memo(App);
