import React from 'react';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import { formatEther } from 'ethers';
import { AiFillQuestionCircle } from 'react-icons/ai';
import { Container, TextField, Modal, Button } from 'src/components';

const FORM_VALIDATION = Yup.object().shape({
  nepCurrency: Yup.number()
    .typeError('Must be in integer format')
    .required('Required'),
  busdCurrency: Yup.number()
    .typeError('Must be in integer format')
    .required('Required'),
});

declare let window: any;

interface IProps {}

const Home: React.FC<IProps> = ({}) => {
  const [showModal, setShowModal] = React.useState(false);

  const initValues = {
    nepCurrency: '',
    busdCurrency: '',
  };

  const [values, setValues] = React.useState({
    userBalance: '',
    userChainId: '',
    userAccountId: '',
    errorMsg: '',
  });

  const { userBalance, userAccountId, userChainId, errorMsg } = values;

  const tableData = [
    {
      id: 0,
      title: 'Account Number',
      value:
        userAccountId.slice(0, 2) +
        '...' +
        userAccountId.slice(
          userAccountId?.length - 4,
          userAccountId.length - 1
        ),
    },
    {
      id: 1,
      title: 'Chain ID',
      value: userChainId,
    },
    {
      id: 0,
      title: 'Balance',
      value: userBalance,
    },
  ];

  const handleSwitchModal = () => setShowModal(!showModal);

  const provider = window.ethereum;

  const connectWallet = React.useCallback(async () => {
    handleSwitchModal();
    try {
      if (typeof window.ethereum !== 'undefined') {
        const chainId = await provider.request({
          method: 'eth_chainId',
        });
        const accounts = await provider.request({
          method: 'eth_requestAccounts',
        });
        const account = accounts[0];
        const userBalance = await provider.request({
          method: 'eth_getBalance',
          params: [account, 'latest'],
        });
        setValues((prev) => ({
          ...prev,
          userBalance: formatEther(userBalance),
          userAccountId: account,
          userChainId: chainId,
        }));
      } else {
        setValues((prev) => ({
          ...prev,
          errorMsg: 'Please install the meta mask extension at first',
        }));
      }
    } catch (err: { message: string }) {
      setValues((prev) => ({
        ...prev,
        errorMsg: err?.message,
      }));
    }
  }, [values, handleSwitchModal]);

  const handleDisconnectWallet = () => {
    if (provider) {
      window.ethereum.request({
        method: 'wallet_requestPermissions',
        params: [{ eth_accounts: {} }],
      });
      setValues((prev) => ({
        ...prev,
        userAccountId: '',
        userBalance: '',
        userChainId: '',
      }));
    }
    handleSwitchModal();
  };

  provider.on('chainChanged', () => window.location.reload());
  return (
    <Container className="flex items-center justify-center absolute inset-0 ">
      <div className="border border-border-color p-6 rounded-xl">
        <div className="flex items-center space-x-0">
          <h3 className="text-2xl font-semibold text-text-black">
            Crypto Converter
          </h3>
          <AiFillQuestionCircle
            color="#0658C0"
            title="1 NEP is equal to 3 BUSD."
            className="mb-5"
          />
        </div>
        <Formik
          initialValues={initValues}
          enableReinitialize
          onSubmit={() => {}}
          validateOnMount
          validationSchema={FORM_VALIDATION}
        >
          {({ setFieldValue, values, handleBlur, handleChange }) => (
            <Form className="my-10">
              <TextField
                label={'NEP'}
                type="number"
                name="nepCurrency"
                onChange={(event: any) => {
                  setFieldValue(
                    'busdCurrency',
                    (event?.target?.value * 3).toFixed(2)
                  ),
                    handleChange(event);
                }}
                onBlur={handleBlur}
                value={values.nepCurrency}
                placeholder="0.00"
              />

              <TextField
                label={'BUSD'}
                type="number"
                name="busdCurrency"
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  const newValue = +event.target.value;
                  setFieldValue('nepCurrency', (newValue / 3).toFixed(2)),
                    handleChange(event);
                }}
                onBlur={handleBlur}
                value={values.busdCurrency}
                placeholder="3.00"
              />
            </Form>
          )}
        </Formik>
        <h5
          onClick={connectWallet}
          className="text-center text-lg text-primary cursor-pointer"
        >
          Connect wallet
        </h5>
      </div>
      {/* modal */}
      {showModal && (
        <Modal
          modalClose={handleSwitchModal}
          onClick={handleSwitchModal}
          title="Crypto Balance"
        >
          {/*  */}
          {errorMsg && (
            <p className="text-warning font-medium text-lg py-5">{errorMsg}</p>
          )}

          {tableData?.map((item) => (
            <div
              className="flex items-center justify-between my-10"
              key={item?.id}
            >
              <p className="font-semibold">{item?.title}</p>
              <p className="text-sm">{item?.value}</p>
            </div>
          ))}
          <div className="flex items-center space-x-5 justify-end pt-5">
            <Button
              variant="secondary"
              text="Disconnect"
              type="button"
              onClick={handleDisconnectWallet}
            />
            <Button
              variant="outline"
              text="Cancel"
              type="button"
              onClick={handleSwitchModal}
            />
          </div>
        </Modal>
      )}
    </Container>
  );
};

export default React.memo(Home);
