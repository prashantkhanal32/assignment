import React from 'react';
import ReactDOM from 'react-dom';
import { MdClose } from 'react-icons/md';

interface Props {
  onClick: () => void;
  children: React.ReactNode;
  title: string;
  modalClose: () => void;
  modalSize?: string;
}

const Modal: React.FC<Props> = ({
  title,
  children,
  modalClose,
  onClick,
  modalSize,
}) => {
  return ReactDOM.createPortal(
    <React.Fragment>
      <div
        className="fixed top-0 left-0 right-0 bottom-0 z-50 bg-[#000] bg-opacity-50  backdrop-blur-[1.5px]"
        onClick={modalClose}
      />
      <div
        className={`${
          modalSize ? `w-[${modalSize}%]` : 'w-[35%]'
        } absolute top-5 right-1/3 h-auto z-50 p-6 rounded-xl bg-pure-white mx-3`}
      >
        <div className="flex items-center justify-between">
          <h5 className="text-xl font-semibold text-text-black">{title}</h5>

          <MdClose onClick={onClick} size={25} className="cursor-pointer" />
        </div>
        <div className="my-3">{children}</div>
      </div>
      <div className="border-bottom" />
    </React.Fragment>,
    document.getElementById('portal') as HTMLElement
  );
};

export default Modal;
