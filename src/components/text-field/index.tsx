import React from 'react';
import { ErrorMessage, FieldHookConfig, useField } from 'formik';
interface IProps {
  label: string | number;
  placeholder?: string;
  type?: string;
  defaultValue?: string | number;
  readOnly?: boolean;
  hidden?: boolean;
  ref?: any;
  onChange?: any;
  onBlur?: any;
}
const TextField = ({
  label,
  placeholder,
  type,
  readOnly,
  defaultValue,
  hidden,
  onChange,
  onBlur,
  ref,
  ...otherProps
}: IProps & FieldHookConfig<string>) => {
  const [field, meta] = useField(otherProps);
  return (
    <div className="mb-4">
      <label
        className={`text-text-black font-semibold text-sm mb-1 block ${
          meta.error && meta.touched ? 'text-[#a94442]' : ''
        } `}
        htmlFor={field.name}
      >
        {label}
      </label>
      <input
        className={`shadow w-full placeholder:text-xs appearance-none ring-1 rounded py-2 block px-3 ring-primary bg-pure-white  focus:outline-none focus:shadow-outline"  ${
          meta.touched && meta.error ? 'ring-[#dc3545] ring-3' : ''
        } ${!meta.error ? 'is-valid' : ''}`}
        {...field}
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        autoComplete="off"
        readOnly={readOnly}
        onChange={onChange}
        onBlur={onBlur}
      />
      <ErrorMessage
        component="div"
        name={field.name}
        className="bg-[#e4655f] text-pure-white mt-1 text-xs inline-block py-1 px-2 mb-3"
      />
    </div>
  );
};

export default React.memo(TextField);
