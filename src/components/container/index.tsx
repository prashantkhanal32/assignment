import React from 'react';

interface IProps {
  children: React.ReactNode;
  className?: string;
}

const Container: React.FC<IProps> = ({ children, className }) => (
  <div
    className={`${className} container lg:max-w-[1180px] md:max-w-[960px] sm:max-w-[720px] w-full mx-auto`}
  >
    {children}
  </div>
);

export default React.memo(Container);
